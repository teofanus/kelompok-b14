from django.urls import path
from .views import home, add_tips, remove_tips, add_news, remove_news, news, tips
from django.conf import settings
from django.conf.urls.static import static

app_name = 'main'

urlpatterns = [
    path('', home, name='home'),
    path('news/', news, name='news'),
    path('tips/', tips, name='tips'),
    path('add-tips/', add_tips, name='add_tips'),
    path('remove-tips/<int:id>', remove_tips, name='remove_tips'),
    path('add-news/', add_news, name='add_news'),
    path('remove-news/<int:id>', remove_news, name='remove_news'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
