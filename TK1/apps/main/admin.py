from django.contrib import admin
from .models import News, Tips

# Register your models here.
admin.site.register(News)
admin.site.register(Tips)
